import {Injectable} from 'angular2/core';
import {Subject} from 'rxjs/Subject'; 

@Injectable()
export class WebSocketTestService {
    private subject: Subject = new Subject();
    private ws : WebSocket;

    connect(_url: string): void {
        let self = this;
        this.ws = new WebSocket(_url);
        this.ws.onopen = function(evt) {
            self.ws.send("Pinging from angular2!");
        };
        this.ws.onclose = function(evt) {};
        this.ws.onmessage = function(evt) { 
            console.log(evt.data);
            self.subject.next(evt.data);
        };
        this.ws.onerror = function(evt) {};
    }

    onMessageSubject(): Subject {
        return this.subject;
    }
}
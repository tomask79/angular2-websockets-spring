import { Component, OnInit } from 'angular2/core';
import { WebSocketTestService } from './websocket.test.service';

@Component({
    selector: 'my-app',
    template: `<h1>WebSocket test by Angular2</h1>
               Last info from server: {{_serverInfo}}`,
    providers: [WebSocketTestService],
})
export class AppComponent implements OnInit {
    public _serverInfo: string;

    constructor(public _webSocketTestService: WebSocketTestService) {
    }

    ngOnInit() {
        this._webSocketTestService.connect
        ("ws://localhost:8080/webSocketTest");
        this._webSocketTestService.onMessageSubject().subscribe(
            value => this._serverInfo = value
        );
    }
}
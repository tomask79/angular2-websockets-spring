package org.websockets.test;

public interface IWebSocketTestHandler {
	void pushEvent() throws Exception;
}

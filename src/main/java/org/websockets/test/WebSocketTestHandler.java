package org.websockets.test;

import java.util.Date;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class WebSocketTestHandler extends TextWebSocketHandler 
	implements IWebSocketTestHandler {
		
	private WebSocketSession session = null;
	
    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) 
    		throws Exception{
    	session.sendMessage(new TextMessage("Spring received: "+message.getPayload()));
    }
    
    @Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
    	this.setSession(session);
	}

	public WebSocketSession getSession() {
		return this.session;
	}

	public void setSession(WebSocketSession session) {
		this.session = session;
	}

	@Scheduled(fixedDelay=1000)
	public void pushEvent() throws Exception {
		String event = "Server info: "+new Date();
		if (getSession() != null && getSession().isOpen()) {
			final ServerInformation serverInformation = new ServerInformation();
			serverInformation.setServerInfo(event);
		
			final ObjectMapper objectMapper = new ObjectMapper();
			getSession().sendMessage(new TextMessage(
				objectMapper.writeValueAsString(serverInformation)
				));
		}
	}
}

package org.websockets.test;

import java.util.Date;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerTest {
	
	@RequestMapping(path="/test", method=RequestMethod.GET)
	public String test() {
		return "Server invoked at: "+new Date();
	}
}

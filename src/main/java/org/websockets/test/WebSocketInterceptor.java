package org.websockets.test;

import java.util.Map;

import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

public class WebSocketInterceptor extends HttpSessionHandshakeInterceptor {

	@Override
	public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response,
			WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
		System.out.println("Request comming: "+request.getURI().toString());
		System.out.println("Handler: "+wsHandler.getClass());
		System.out.println("Response: "+response.getBody());
		return super.beforeHandshake(request, response, wsHandler, attributes);
	}


	@Override
	public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response,
			WebSocketHandler wsHandler, Exception ex) {
		System.out.println("After handshake: "+wsHandler.getClass());
	}
}

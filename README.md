# Writing applications in Angular2, [part 16] #

## Creation of Spring-WebSocket application with Angular 2 client  ##

Finally I found a time to play a little bit with raw [WebSockets](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/websocket.html) support in Spring framework. As web-socket client we'll create simple [Angular2](https://angular.io/) application serving pushed data from server via RxJs [Observables](https://coryrylan.com/blog/angular-2-observable-data-services).

## WebSocket backend powered by Spring Framework ##

Just very briefly, WebSocket is an protocol allowing **both-direction** communication between client and server via single TCP connection. And why you should consider using of WebSockets? Always when server should return to client **asynchronously** some result of long running operation...

### Step 1) Dispatcher servlet ###

Most of the time, you want your Spring backend to support REST and WebSockets together, luckily [DispatcherServlet](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/mvc.html#mvc-servlet) allows this without any problems, so let's prepare him for annotation based application.

```
public class MyWebInitializer implements WebApplicationInitializer {

	public void onStartup(ServletContext servletContext)
			throws ServletException {
        WebApplicationContext context = getContext();
        servletContext.addListener(new ContextLoaderListener(context));
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet(
        		"DispatcherServlet", new DispatcherServlet(context));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/*");
	}
	
    private AnnotationConfigWebApplicationContext getContext() {
        AnnotationConfigWebApplicationContext context = 
        		new AnnotationConfigWebApplicationContext();
        context.register(WebSocketConfig.class);
        context.register(WebMvcConfig.class);
        return context;
    }
}
```
WebSocketConfig and WebMvcConfig are configuration containers for beans. In **WebMvcConfig** let's say to DispatcherServlet that every request to "/" path should be redirected to "index.html" page and let's also map resources of our Angular2 application in WebApp folder.

### Step 2) Mapping of Angular2 resources ###

```
@EnableWebMvc
@EnableScheduling
@ComponentScan("org.websockets.test")
public class WebMvcConfig extends WebMvcConfigurerAdapter{
	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/**").addResourceLocations("/");
        registry.addResourceHandler("/app/**").addResourceLocations("/app/");        
	}
		
	@Override
	public void addViewControllers(final ViewControllerRegistry registry) {
	    registry.addViewController("/").setViewName("forward:/index.html");
	}
}
```
By row: 

```
registry.addResourceHandler("/**").addResourceLocations("/")   

```
we're opening access to resources in the WebApp folder. So for example, if we've got index.html in the WebApp folder then index.html will be now accessible from http://domain:port/index.html. Make sense? Now to WebSocketConfig:

### Step 3) Mapping of WebSocket handler and interceptors ###

```
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(myHandler(), "/webSocketTest").addInterceptors(
        		new WebSocketInterceptor());
	}
	
	@Bean
	public WebSocketHandler myHandler() {
	    return new WebSocketTestHandler();
	}
}
```
By this Spring configuration we're enabling WebSockets and map them to URL **ws://domain:port/webSocketTest**. You also can attach WebSocket handshake interceptor like I did in case of WebSocketInterceptor. For more info check perfect Spring documentation for [WebSockets](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/websocket.html). 

### Step 4) WebSocket handler and pushing of server data ###

```
package org.websockets.test;

import java.util.Date;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class WebSocketTestHandler extends TextWebSocketHandler 
	implements IWebSocketTestHandler {
		
	private WebSocketSession session = null;
	
    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) 
    		throws Exception{
    	session.sendMessage(new TextMessage("Spring received: "+message.getPayload()));
    }
    
    @Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
    	this.setSession(session);
	}

	public WebSocketSession getSession() {
		return this.session;
	}

	public void setSession(WebSocketSession session) {
		this.session = session;
	}

	@Scheduled(fixedDelay=1000)
	public void pushEvent() throws Exception {
		String event = "Server info: "+new Date();
		if (getSession() != null && getSession().isOpen()) {
			final ServerInformation serverInformation = new ServerInformation();
			serverInformation.setServerInfo(event);
		
			final ObjectMapper objectMapper = new ObjectMapper();
			getSession().sendMessage(new TextMessage(
				objectMapper.writeValueAsString(serverInformation)
				));
		}
	}
}
```
This class simply:

* **Saves and keeps session of only last connected WebSocket client**. But it should'n be a problem for you to add a Map holding sessions of all clients so you'd know all the clients you should let know about particular server event. 

* On every received message from WebSocket client it returns back "Spring received" plus received payload, see method handleTextMessage.

* Every 1 second it pushes to client string "Server info: " plus server time. See method, pushEvent.

### Step 5) Testing whether WebSockets works ###

To test whether WebSockets works at your environment I attached into WebApp folder file **webSocket.html** which will ping previously created WebSocket endpoint. So perform the following:

* mvn clean install (in the root folder with pom.xml)
* mvn tomcat7:run-war
* from browser hit http://localhost:8080/webSocket.html

you should receive:

```
WebSocket Test

CONNECTED

SENT: WebSocket rocks

RESPONSE: Spring received: WebSocket rocks

DISCONNECTED
```
If it ended like this then your environment is ready!

## Angular2 WebSocket Client with Observables  ##

Before any Angular2 component, let's create an Observable based service which will expose Subject for getting WebSocket data pushed from server:

```
import {Injectable} from 'angular2/core';
import {Subject} from 'rxjs/Subject'; 

@Injectable()
export class WebSocketTestService {
    private subject: Subject = new Subject();
    private ws : WebSocket;

    connect(_url: string): void {
        let self = this;
        this.ws = new WebSocket(_url);
        this.ws.onopen = function(evt) {
            self.ws.send("Pinging from angular2!");
        };
        this.ws.onclose = function(evt) {};
        this.ws.onmessage = function(evt) { 
            console.log(evt.data);
            self.subject.next(evt.data);
        };
        this.ws.onerror = function(evt) {};
    }

    onMessageSubject(): Subject {
        return this.subject;
    }
}
```
Now let's create Angular2 component for displaying pushed WebSocket data. This component will subscribe on Subject returned by onMessageSubject function.


```
import { Component, OnInit } from 'angular2/core';
import { WebSocketTestService } from './websocket.test.service';

@Component({
    selector: 'my-app',
    template: `<h1>WebSocket test by Angular2</h1>
               Last info from server: {{_serverInfo}}`,
    providers: [WebSocketTestService],
})
export class AppComponent implements OnInit {
    public _serverInfo: string;

    constructor(public _webSocketTestService: WebSocketTestService) {
    }

    ngOnInit() {
        this._webSocketTestService.connect
        ("ws://localhost:8080/webSocketTest");
        this._webSocketTestService.onMessageSubject().subscribe(
            value => this._serverInfo = value
        );
    }
}
```

* This component will get injected WebSocketTestService as singleton into _webSocketTestService field.

* In ngOInit [Life Cycle Hook](https://angular.io/docs/ts/latest/guide/lifecycle-hooks.html) we connected to Spring WebSocket backend and subscribed to any returned data from WebSocket.

### Testing the Angular2 client ###

* mvn clean install (in the root folder with pom.xml)
* mvn tomcat7:run-war
* in the browser hit http://localhost:8080/

You should see then result like:

```
WebSocket test by Angular2
Last info from server: {"serverInfo":"Server info: Wed Aug 24 22:46:47 CEST 2016"}
```
changing every second.

One note: 

You noticed that I didn't compiled typescript files into javascript files before running the project. It's because they're transpiled on the fly in the browser. You can effort this in the easy applications, but in the big applications use one of the mentioned approaches in this excellent article: http://blog.mgechev.com/2016/06/26/tree-shaking-angular2-production-build-rollup-javascript/

regards

Tomas
 








